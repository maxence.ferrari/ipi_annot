BOMBYX IPI extractor
====================

`IPI_bombyx.py` is a graphic utility made for the manual extraction of sperm whale inter pulse interval (IPI).
It builds on the GUI of `ipi_extract.py`. The main features are thus explained in the corresponding [Read me](README.md).
Some of the explanations here are specific to the member of the Dyni team working on this project.

Option
------
You can obtain a list of all the available options by typing
`python IPI_bombyx.py -h`


Usage
-----

If you're executing this script on one of the cubes, you can simply launch the script by typing

`python path/to/passage_maxpred.pkl your_name_here`

If you're on another server, you will need to set the `--wd` and `--done_file` options to the path of the BOMBYX database, and the shared *done_file.csv* 

### Number of done files

The number of current files that have been labeled is displayed on the bottom right, 
along with the number of files that have been labeled by the current user. Note that labeled files that are not finished are not included in the count. 


### Next button
Once you have finished labeling the current file, you can click on the `Next file` button to load and annotate the next file.
The database will be updated with the labels of the current file, and the shared `done_file.csv` will include the corresponding passage number.

### Closing the window
If you close the window, the click for the current file will be saved to the database.
However, the current file will not be considered as a file completely labeled. 
Thus, the shared `done_file.csv`

### Output file

This script will save the same kind of HDF file as `ipi_extract.py`. 
However, unlinked `ipi_extract.py` which output one file per annotated file,
this script will only create one database per annotator. 
Changing the selling of the annotator will change the database associated.

In addition, of the columns saved in `ipi_extract.py`, this script also saves the file, passage number, and annotator name.

done_file.csv
-------------

This file is used to record the progression of the annotation among all the users.
It contained the passage number of each passage that has been labeled.


If you don't know what you are doing, <span style="color:red">__&#x26a0;DO NOT TOUCH OR MODIFY THIS FILE&#x26a0;__</span>.
